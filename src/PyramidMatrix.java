
public class PyramidMatrix {
    private int matrixSize;
    private int[][] matrix;
    private int currentLeftUpCornerIndex[];
    private int currentRightUpCornerIndex[];
    private int currentLeftDownCornerIndex[];
    private int currentRightDownCornerIndex[];
    private boolean matrixIsFull;
    private int currentColumn;
    private int currentValue;
    private int currentRow;

    public PyramidMatrix(Integer matrixSize) {
        this.matrixSize = matrixSize;
        this.matrix = new int[matrixSize][matrixSize];
        this.currentLeftUpCornerIndex = new int[]{0, 0};
        this.currentRightUpCornerIndex = new int[]{0, matrixSize - 1};
        this.currentLeftDownCornerIndex = new int[]{matrixSize - 1, 0};
        this.currentRightDownCornerIndex = new int[]{matrixSize - 1, matrixSize - 1};
        this.matrixIsFull = false;
        this.currentColumn = 0;
        this.currentRow = 0;
        this.currentValue = 1;
    }

    public void setMatrix() {
        while (!matrixIsFull) {
            fillRawToRight();
            fillColumnDown();
            fillRawToLeft();
            fillColumnUp();
            this.currentValue++;
        }
        showMatrix();
    }

    private void showMatrix() {
        int maxNumber = (int) Math.round(matrixSize / 2.0);
        int maxNumberLength = String.valueOf(maxNumber).length();
        for (int rowIndex = 0; rowIndex < matrixSize; rowIndex++) {
            for (int columnIndex = 0; columnIndex < matrixSize; columnIndex++) {
                int currentNumber = matrix[rowIndex][columnIndex];
                String result = mapNumberToString(currentNumber, maxNumberLength);
                System.out.print(result + " ");
            }
            System.out.println();
        }
    }

    private String mapNumberToString(int number, int maxNumberLength) {
        String result = "";
        int numOfAddDigits = maxNumberLength - String.valueOf(number).length();
        if (numOfAddDigits == 1) result = "0" + number;
        else if (numOfAddDigits == 2) result = "00" + number;
        else result = String.valueOf(number);
        return result;
    }

    private void fillRawToRight() {
        if ((matrix[currentRow][currentColumn]) != 0) {
            matrixIsFull = true;
        } else {
            for (int index = currentColumn; index <= currentRightUpCornerIndex[1]; index++) {
                this.matrix[currentRow][index] = currentValue;
            }
        }
        this.currentRow = currentRightUpCornerIndex[0] + 1;
        this.currentColumn = currentRightUpCornerIndex[1];
        this.currentRightUpCornerIndex[0] = currentRightUpCornerIndex[0] + 1;
        this.currentRightUpCornerIndex[1] = currentRightUpCornerIndex[1] - 1;
    }

    public void fillColumnDown() {
        if ((matrix[currentRow][currentColumn]) != 0) {
            matrixIsFull = true;
        } else {
            for (int index = currentRow; index <= currentRightDownCornerIndex[0]; index++) {
                this.matrix[index][currentColumn] = this.currentValue;
            }
        }
        this.currentRow = currentRightDownCornerIndex[0];
        this.currentColumn = currentRightDownCornerIndex[1] - 1;
        this.currentRightDownCornerIndex[0] = currentRightDownCornerIndex[0] - 1;
        this.currentRightDownCornerIndex[1] = currentRightDownCornerIndex[1] - 1;
    }

    public void fillRawToLeft() {
        if ((matrix[currentRow][currentColumn]) != 0) {
            matrixIsFull = true;
        } else {
            for (int index = currentColumn; index >= currentLeftDownCornerIndex[1]; index--) {
                this.matrix[currentRow][index] = currentValue;
            }
        }
        this.currentRow = currentLeftDownCornerIndex[0] - 1;
        this.currentColumn = currentLeftDownCornerIndex[1];
        this.currentLeftDownCornerIndex[0] = currentLeftDownCornerIndex[0] - 1;
        this.currentLeftDownCornerIndex[1] = currentLeftDownCornerIndex[1] + 1;
    }

    public void fillColumnUp() {
        if ((matrix[currentRow][currentColumn]) != 0) {
            matrixIsFull = true;
        } else {
            for (int index = currentRow; index >= currentLeftUpCornerIndex[0]; index--) {
                this.matrix[index][currentColumn] = currentValue;
            }
        }
        this.currentRow = currentLeftUpCornerIndex[0] + 1;
        this.currentColumn = currentLeftUpCornerIndex[1] + 1;
        this.currentLeftUpCornerIndex[0] = currentLeftUpCornerIndex[0] + 1;
        this.currentLeftUpCornerIndex[1] = currentLeftUpCornerIndex[1] + 1;
    }
}
