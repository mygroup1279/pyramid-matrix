import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static final int MAX_MATRIX_SIZE = 1999;

    public static void main(String[] args) {
        boolean isValid = false;
        int matrixSize = 0;
        Scanner scanner = new Scanner(System.in);
        while (!isValid) {
            System.out.println("Enter matrix size: ");
            try {
                matrixSize = scanner.nextInt();
                if (matrixSize <= 1 || matrixSize > MAX_MATRIX_SIZE) {
                    System.out.println("Matrix size is not valid. Try again.");
                } else {
                    isValid = true;
                    scanner.close();
                }
            } catch (InputMismatchException e) {
                System.out.println("Enter integer.");
            }
        }
        PyramidMatrix snakeMatrix = new PyramidMatrix(matrixSize);
        snakeMatrix.setMatrix();
    }
}